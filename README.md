<p align="center">
  <img alt="bloom logo" src="https://bloom.sh/kernel/static/imgs/logos/bloom_256.png" height="180" />
  <h3 align="center">Bloom - apps</h3>
  <p align="center">Empowering the world with open technologies</p>
</p>

[Try it online for free](https://bloom.sh)

--------


[Available on Google play store](https://play.google.com/store/apps/details?id=com.bloom42.bloomx)

1. [Contributing](#contributing)
2. [Licensing](#licensing)
3. [Sponsoring](#sponsoring)
4. [Security](#security)

--------

## Contributing

Thank you for your interest in contributing! Please refer to
[https://bloom.sh/contribute](https://bloom.sh/contribute) for guidance.



## Licensing

See `LICENSE.txt` and [https://bloom.sh/licensing](https://bloom.sh/licensing)


## Sponsoring

Bloom is a free and open source project. If you are interested in supporting this project, the core team
and the contributors please visit our
[dedicated 'Become a sponsor' page](https://bloom.sh/become-a-sponsor) ✌️


## Security

If you found a security issue affecting this project, please do not open a public issue and refer to our
[dedicated security page](https://bloom.sh/security) instead. Thank you.
